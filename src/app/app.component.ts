import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { BackgroundService } from './core/services/background.service';

@Component({
  selector: 'bbc-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'blue-board-client';
  backgroundImagePath: string;
  backgroundColor: string;
  backgroundStyle: {'background-image'?: string, 'background-color'?: string } = {};

  constructor(private translate: TranslateService, private backgroundService: BackgroundService) {
  }

  ngOnInit() {
    this.translate.setDefaultLang('en');
    this.translate.getTranslation('he');
    this.translate.use('he');

    this.backgroundService.backgroundChangeSubject.subscribe(backgroundCover => {
      const { imagePath, color } = backgroundCover;
      this.backgroundStyle = {};

      if (imagePath) this.backgroundStyle['background-image'] = `url(${imagePath})`;
      if (color) this.backgroundStyle['background-color'] = color;
    });
  }
}
