import { Team } from './team.model';
import { User } from './user.model';

export enum ListPermissionType {
    SEE = 'GET',
    MOVE_IN = 'UPDATE',
    MOVE_OUT = 'UPDATE',
    CREATE = 'CREATE',
    DELETE = 'DELETE'
}

export interface ListPermission {
    permissionType: ListPermissionType,
    user: User | Team,
}