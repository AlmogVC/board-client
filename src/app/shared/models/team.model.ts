import { User } from './user.model';
import { Label } from './label.model';
import { Board } from './board.model';

export interface Team {
    id?: string;
    name: string;
    description?: string;
    creator: User | string;
    creationDate: Date;
    updateDate: Date;
    labels?: Label[];
    members?: User[];
    boards?: Board[] | string[];
}