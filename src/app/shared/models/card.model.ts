import { User } from './user.model';
import { Label } from './label.model';

export interface Card {
    id?: string;
    title: string;
    description?: string;
    creationDate: Date;
    updateDate: Date;
    user: string | User;
    assignees?: User[] | string[];
    cover?: string;
    labels?: string[] | Label[];
    dueDate?: Date;
}