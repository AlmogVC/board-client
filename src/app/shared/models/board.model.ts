import { List } from './list.model';
import { Team } from './team.model';
import { User } from './user.model';
import { Label } from './label.model';

export interface Board {
    id?: string;
    title: string;
    creationDate: Date;
    updateDate: Date;
    lists: List[];
    labels?: Label[];
    user?: User | string;
    team?: Team | string;
    cover?: string;
}