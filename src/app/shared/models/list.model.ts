import { Card } from './card.model';
import { Label } from './label.model';
import { ListPermission } from './permission.model';

export interface List {
    id?: string;
    title: string;
    cards: Card[];
    labels?: Label[];
    permissions?: ListPermission[]
}