import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { Card } from 'src/app/shared/models/card.model';
import { cardMocks } from '../mocks/card.mock';
import { userMocks } from '../mocks/user.mock';

@Injectable({
  providedIn: 'root'
})
export class CardService {
  constructor(private http: HttpClient) { }

  getById(cardId: string): Observable<Card> {
    return of<Card>(cardMocks.find(card => card.id === cardId));
  }

  create(name: string) {
    const card: Card = {
      title: name,
      creationDate: new Date(),
      updateDate: new Date(),
      user: userMocks[0],
      id: (cardMocks.length + 1).toString(),
    };

    cardMocks.push(card);

    return of<Card>(card);
  }
}
