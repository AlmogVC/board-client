import { Injectable } from '@angular/core';
import { of, Observable } from 'rxjs';
import { teamMocks } from '../mocks/team.mock';
import { Team } from 'src/app/shared/models/team.model';

@Injectable({
  providedIn: 'root'
})
export class TeamService {

  constructor() { }

  getMany(): Observable<Team[]> {
    return of(teamMocks);
  }
}
