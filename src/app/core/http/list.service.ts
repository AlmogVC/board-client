import { Injectable } from '@angular/core';
import { listMocks } from '../mocks/list.mock';
import { of, from } from 'rxjs';
import { List } from 'src/app/shared/models/list.model';
import { Card } from 'src/app/shared/models/card.model';

@Injectable({
  providedIn: 'root'
})
export class ListService {

  constructor() { }

  create(title: string) {
    const newList: List = { cards: [], title, id: (Math.round(Math.random() * 10000)).toString() };
    listMocks.push(newList);

    return of(newList);
  }

  getMany() {
    return of(listMocks);
  }

  addCard(listId: string, card: Card) {
    for (let listIndex = 0; listIndex < listMocks.length; listIndex++) {
      if (listMocks[listIndex].id === listId) {
        listMocks[listIndex].cards.push(card);
      }
    }

    return of(listMocks);
  }
}
