import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LabelService {
  nameVisibilitySubject = new Subject<boolean>();
  public isNameVisible = false;

  constructor() {
    this.nameVisibilitySubject.subscribe(isNameVisible => this.isNameVisible = isNameVisible);
  }
}
