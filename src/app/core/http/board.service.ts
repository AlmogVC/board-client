import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { boardMocks } from '../mocks/board.mock';
import { of, Observable } from 'rxjs';
import { Board } from 'src/app/shared/models/board.model';

@Injectable({
  providedIn: 'root'
})
export class BoardService {
  constructor(private http: HttpClient) { }

  getById(boardId: string): Observable<Board> {
    return of<Board>(boardMocks.find(board => board.id === boardId));
  }

  getMany(): Observable<Board[]> {
    return of<Board[]>(boardMocks);
  }
}
