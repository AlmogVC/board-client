import { Card } from 'src/app/shared/models/card.model';
import { userMocks } from './user.mock';
import { labelMocks } from './labels.mock';

export const cardMocks: Card[] = [
    {
        id: '1',
        title: ' להוסיף תמונה לכרטיסים אחרי שמנסים לעשות הרבה שורות כדי שנרד שורה',
        creationDate: new Date(),
        updateDate: new Date(),
        user: userMocks[0],
        description: `
        # להוסיף תמונה לכרטיסים
        ## להוסיף תמונה לכל כרטיס אפשרי
        ### איזה כיף
        [x] משהו
        `,
        assignees: [userMocks[0], userMocks[1], userMocks[0], userMocks[1], userMocks[2]],
        labels: [labelMocks[0], labelMocks[1], labelMocks[2], labelMocks[3]],
        dueDate: new Date(),
    },
    {
        id: '2',
        title: 'test 2',
        creationDate: new Date(),
        updateDate: new Date(),
        user: userMocks[0],
        description: '',
        cover: 'assets/images/test/shiba.jpg',
        assignees: [
            userMocks[0], userMocks[1],
            userMocks[2], userMocks[0],
            userMocks[1], userMocks[2],
            userMocks[0], userMocks[1],
            userMocks[2]
        ],
        labels: [labelMocks[0]],
    },
    {
        id: '3',
        title: 'test 3',
        creationDate: new Date(),
        updateDate: new Date(),
        user: userMocks[0],
        description: '',
        dueDate: new Date(),
        assignees: [userMocks[0]],
        labels: [labelMocks[0], labelMocks[1], labelMocks[2], labelMocks[3], labelMocks[1], labelMocks[2], labelMocks[1], labelMocks[2],],
    },
    {
        id: '4',
        title: 'test 4',
        creationDate: new Date(),
        updateDate: new Date(),
        user: userMocks[0],
        description: '',
        assignees: [userMocks[0], userMocks[1]],
    },
    {
        id: '5',
        title: 'test 5',
        creationDate: new Date(),
        updateDate: new Date(),
        user: userMocks[0],
        description: '',
        cover: 'assets/images/test/building.jpg',
        assignees: [userMocks[0]],
    },
    {
        id: '6',
        title: 'test 6',
        creationDate: new Date(),
        updateDate: new Date(),
        user: userMocks[0],
        description: '',
    },
    {
        id: '7',
        title: 'test 7',
        creationDate: new Date(),
        updateDate: new Date(),
        user: userMocks[0],
        description: '',
    },
    {
        id: '8',
        title: 'test 8',
        creationDate: new Date(),
        updateDate: new Date(),
        user: userMocks[0],
        description: '',
    },
];
