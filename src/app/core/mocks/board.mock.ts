import { Board } from 'src/app/shared/models/board.model';
import { listMocks } from './list.mock';
import { labelMocks } from './labels.mock';

export const boardMocks: Board[] = [
    {   
        id: '1',
        title: 'Board 1',
        lists: [...listMocks],
        creationDate: new Date(),
        updateDate: new Date(),
        labels: [labelMocks[0], labelMocks[1], labelMocks[2]],
        cover: 'assets/images/test/wallpaper_1.jpg',
    },
    {
        id: '2',
        title: 'ניסוי',
        lists: [...listMocks],
        creationDate: new Date(),
        updateDate: new Date(),
        labels: [labelMocks[0], labelMocks[1], labelMocks[2]],
        cover: 'assets/images/test/wallpaper_2.jpg',
    },
    {
        id: '3',
        title: 'ניסוי',
        lists: [...listMocks],
        creationDate: new Date(),
        updateDate: new Date(),
        labels: [labelMocks[0], labelMocks[1], labelMocks[2]],
        cover: 'assets/images/test/wallpaper_3.jpg',
    },
    {
        id: '4',
        title: 'ניסוי',
        lists: [...listMocks],
        creationDate: new Date(),
        updateDate: new Date(),
        labels: [labelMocks[0], labelMocks[1], labelMocks[2]],
        cover: 'assets/images/test/wallpaper_3.jpg',
    },
    {
        id: '5',
        title: 'ניסוי',
        lists: [...listMocks],
        creationDate: new Date(),
        updateDate: new Date(),
        labels: [labelMocks[0], labelMocks[1], labelMocks[2]],
        cover: 'assets/images/test/wallpaper_3.jpg',
    },
];
