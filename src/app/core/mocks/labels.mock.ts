import { Board } from 'src/app/shared/models/board.model';
import { listMocks } from './list.mock';
import { Label } from 'src/app/shared/models/label.model';

export const labelMocks: Label[] = [
    {
        color: '#2ecc71',
        name: 'feature',
        id: '1',
    },
    {
        color: '#e74c3c',
        name: 'fix',
        id: '2',
    },
    {
        color: '#2c3e50',
        name: 'build',
        id: '3',
    },
    {
        color: '#f1c40f',
        name: 'ci',
        id: '4',
    },
    {
        color: '#8e44ad',
        name: 'refactor',
        id: '5',
    },
];
