import { Team } from 'src/app/shared/models/team.model';
import { userMocks } from './user.mock';
import { boardMocks } from './board.mock';

export const teamMocks: Team[] = [
    {
        creationDate: new Date(),
        updateDate: new Date(),
        creator: userMocks[0],
        name: 'קבוצת היפים',
        id: '1',
        boards: [boardMocks[0], boardMocks[1]],
    },
    {
        creationDate: new Date(),
        updateDate: new Date(),
        creator: userMocks[0],
        name: 'קבוצת האמיצים',
        id: '2',
        boards: [boardMocks[2]],
    },
    {
        creationDate: new Date(),
        updateDate: new Date(),
        creator: userMocks[0],
        name: 'קבוצת המתאמצים',
        id: '2',
        boards: [boardMocks[3], boardMocks[4]],
    },
];