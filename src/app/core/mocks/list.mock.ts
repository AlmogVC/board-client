import { List } from 'src/app/shared/models/list.model';
import { cardMocks } from './card.mock';
import { labelMocks } from './labels.mock';

export const listMocks: List[] = [
    {
        id: '11',
        title: 'דברים לעשות',
        cards: [
            cardMocks[0],
            cardMocks[1],
            cardMocks[2],
        ],
        labels: [labelMocks[0]],
    },
    {
        id: '12',
        title: 'עושה עכשיו',
        cards: [
            cardMocks[3],
        ],
    },
    {
        id: '13',
        title: 'עשיתי',
        cards: [
            cardMocks[4],
            cardMocks[5],
        ],
    },
    {
        id: '14',
        title: 'לעשות בעתיד הרחוק',
        cards: [
            cardMocks[6],
            cardMocks[7]
        ],
        labels: [labelMocks[0], labelMocks[1], labelMocks[3]],
    },
];
