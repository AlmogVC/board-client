import { User } from 'src/app/shared/models/user.model';

export const userMocks: User[] = [
    {
        firstName: 'אלמוג',
        lastName: 'וגמן ציפרוט',
        id: '1',
    },
    {
        firstName: 'רונאלד',
        lastName: 'כהן',
        id: '2',
    },
    {
        firstName: 'אביגדור',
        lastName: 'יחסקל',
        id: '3',
    },
    {
        firstName: 'שמעון',
        lastName: 'גלעדי',
        id: '4',
    },
];
