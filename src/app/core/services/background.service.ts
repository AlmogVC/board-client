import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

export type BackgroundCover = { color?: string, imagePath?: string };
@Injectable({
  providedIn: 'root'
})
export class BackgroundService {
  private backgroundImagePath: string;
  private backgroundColor: string;

  backgroundChangeSubject: Subject<BackgroundCover> = new Subject<BackgroundCover>();

  constructor() { }

  setBackgroundImage(imagePath: string) {
    this.backgroundColor = undefined;
    this.backgroundImagePath = imagePath;
    this.backgroundChangeSubject.next({ imagePath });
  }

  setBackgroundColor(color: string) {
    this.backgroundImagePath = undefined;
    this.backgroundColor = color;
    this.backgroundChangeSubject.next({ color });
  }

  getBackgroundImage() {
    return this.backgroundImagePath;
  }

  getBackgroundColor() {
    return this.backgroundColor;
  }
}
