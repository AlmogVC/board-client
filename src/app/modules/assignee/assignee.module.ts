import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AssigneeComponent } from './components/assignee/assignee.component';
import { AssigneeListComponent } from './components/assignee-list/assignee-list.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { AssigneeLimitIndicatorComponent } from './components/assignee-limit-indicator/assignee-limit-indicator.component';



@NgModule({
  declarations: [AssigneeComponent, AssigneeListComponent, AssigneeLimitIndicatorComponent],
  imports: [
    CommonModule,
    SharedModule,
  ],
  exports: [AssigneeListComponent]
})
export class AssigneeModule { }
