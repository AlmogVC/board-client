import { Component, OnInit, Input } from '@angular/core';
import { User } from 'src/app/shared/models/user.model';

@Component({
  selector: 'bbc-assignee',
  templateUrl: './assignee.component.html',
  styleUrls: ['./assignee.component.scss']
})
export class AssigneeComponent implements OnInit {
  @Input() assignee: User;
  shortName: string;
  fullName: string;

  constructor() { }

  ngOnInit(): void {
    this.initShortName();
    this.fullName = `${this.assignee.firstName} ${this.assignee.lastName}`;
  }

  initShortName() {
    this.shortName = this.assignee.firstName.charAt(0) + this.assignee.lastName.charAt(0);
  }

}
