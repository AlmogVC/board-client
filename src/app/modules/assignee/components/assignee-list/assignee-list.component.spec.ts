import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { AssigneeListComponent } from './assignee-list.component';

describe('AssigneeListComponent', () => {
  let component: AssigneeListComponent;
  let fixture: ComponentFixture<AssigneeListComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ AssigneeListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssigneeListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
