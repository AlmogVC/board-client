import { Component, OnInit, Input } from '@angular/core';
import { User } from 'src/app/shared/models/user.model';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'bbc-assignee-list',
  templateUrl: './assignee-list.component.html',
  styleUrls: ['./assignee-list.component.scss']
})
export class AssigneeListComponent implements OnInit {
  @Input() assignees: User[] = [];
  assigneeToDisplay: User[] = [];
  assigneeNotDisplayed: User[] = [];

  wasAssigneeDisplayLimitReached = false;

  constructor() { }

  ngOnInit(): void {
    if (this.assignees?.length > environment.assigneeCardDisplayLimit) {
      this.wasAssigneeDisplayLimitReached = true;
      this.assigneeToDisplay = this.assignees.slice(0, environment.assigneeCardDisplayLimit);
      this.assigneeNotDisplayed = this.assignees.slice(environment.assigneeCardDisplayLimit, this.assignees.length);
    } else {
      this.assigneeToDisplay = this.assignees;
    }
  }

}
