import { Component, OnInit, Input } from '@angular/core';
import { User } from 'src/app/shared/models/user.model';

@Component({
  selector: 'bbc-assignee-limit-indicator',
  templateUrl: './assignee-limit-indicator.component.html',
  styleUrls: ['./assignee-limit-indicator.component.scss']
})
export class AssigneeLimitIndicatorComponent implements OnInit {
  @Input() assignees: User[] = [];
  assigneeNames: string;

  constructor() { }

  ngOnInit(): void {
    this.assigneeNames = this.assignees.map(assignee => `${assignee.firstName} ${assignee.lastName}`).join('\n');
  }
}
