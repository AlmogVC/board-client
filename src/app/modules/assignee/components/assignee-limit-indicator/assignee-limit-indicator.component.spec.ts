import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { AssigneeLimitIndicatorComponent } from './assignee-limit-indicator.component';

describe('AssigneeLimitIndicatorComponent', () => {
  let component: AssigneeLimitIndicatorComponent;
  let fixture: ComponentFixture<AssigneeLimitIndicatorComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ AssigneeLimitIndicatorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssigneeLimitIndicatorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
