import { Component, OnInit, Input } from '@angular/core';
import { List } from 'src/app/shared/models/list.model';

@Component({
  selector: 'bbc-list-header',
  templateUrl: './list-header.component.html',
  styleUrls: ['./list-header.component.scss']
})
export class ListHeaderComponent implements OnInit {
  @Input() list: List;

  constructor() { }

  ngOnInit(): void {
  }

}
