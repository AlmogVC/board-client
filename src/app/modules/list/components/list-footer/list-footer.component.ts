import { Component, OnInit, Input } from '@angular/core';
import { List } from 'src/app/shared/models/list.model';

@Component({
  selector: 'bbc-list-footer',
  templateUrl: './list-footer.component.html',
  styleUrls: ['./list-footer.component.scss']
})
export class ListFooterComponent implements OnInit {
  @Input() list: List;
  showCardForm: boolean = false;

  constructor() { }

  ngOnInit(): void {
  }

  onClickAddCard() {
    this.showCardForm = true;
  }

  closeAddCard() {
    this.showCardForm = false;
  }
}
