import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import { CdkDragDrop, moveItemInArray, transferArrayItem} from '@angular/cdk/drag-drop';

import { List } from 'src/app/shared/models/list.model';

@Component({
  selector: 'bbc-lists',
  templateUrl: './lists.component.html',
  styleUrls: ['./lists.component.scss']
})
export class ListsComponent implements OnChanges {
  @Input() lists: List[] = [];
  listIds: string[] = [];

  constructor() { }

  ngOnChanges(): void {
    this.listIds = this.lists.map(list => list.id);
  }

  onDrag(event: CdkDragDrop<string[]>) {
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    }
  }
}
