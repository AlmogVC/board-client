import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'bbc-list-add',
  templateUrl: './list-add.component.html',
  styleUrls: ['./list-add.component.scss']
})
export class ListAddComponent implements OnInit {
  @Output() listCreated: EventEmitter<string> = new EventEmitter<string>();

  displayInput = false;
  listName: FormControl = new FormControl('');

  constructor() { }

  ngOnInit(): void {
  }

  onClick() {
    this.displayInput = true;
  }

  addNewList() {
    this.listCreated.next(this.listName.value)
  }
}
