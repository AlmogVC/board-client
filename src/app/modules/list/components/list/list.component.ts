import { Component, OnInit, Input } from '@angular/core';
import { CdkDragDrop, moveItemInArray, transferArrayItem} from '@angular/cdk/drag-drop';
import { List } from 'src/app/shared/models/list.model';
import { Label } from 'src/app/shared/models/label.model';

@Component({
  selector: 'bbc-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {
  @Input() list: List;
  @Input() connectedListsIds: string[] = [];

  constructor() { }

  ngOnInit(): void {
  }

  onCardMove(event: any) {
    this.assignLabelsToCards();
  }

  assignLabelsToCards() {
    if (this.list.labels) {
      this.list.cards = this.list.cards.map(card => {
        const cardLabels: Label[] = card.labels as Label[] || [];
        const labels: Label[] = [...cardLabels, ...this.list.labels as Label[]];

        card.labels = Array.from(new Set<Label>(labels));

        return card;
      });
    }
  }
}
