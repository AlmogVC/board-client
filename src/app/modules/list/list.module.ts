import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DragDropModule } from '@angular/cdk/drag-drop'; 

import { ListRoutingModule } from './list-routing.module';
import { ListComponent } from './components/list/list.component';
import { ListsComponent } from './components/lists/lists.component';
import { CardModule } from '../card/card.module';
import { ListAddComponent } from './components/list-add/list-add.component';
import { ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from 'src/app/shared/shared.module';
import { LabelModule } from '../label/label.module';
import { ListFooterComponent } from './components/list-footer/list-footer.component';
import { ListHeaderComponent } from './components/list-header/list-header.component';


@NgModule({
  declarations: [ListComponent, ListsComponent, ListAddComponent, ListFooterComponent, ListHeaderComponent],
  imports: [
    CommonModule,
    SharedModule,
    ListRoutingModule,
    CardModule,
    DragDropModule,
    ReactiveFormsModule,
    LabelModule,
  ],
  exports: [ListsComponent, ListAddComponent],
})
export class ListModule { }
