import { Component, OnInit, Input } from '@angular/core';

import { Card } from 'src/app/shared/models/card.model';
import { Router } from '@angular/router';

@Component({
  selector: 'bbc-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnInit {
  @Input() card: Card;

  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  onClick(): void {
    this.router.navigate(['board/1', { outlets: { card: ['card', this.card.id] } }]);
  }
}
