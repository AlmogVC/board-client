import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { FormControl } from '@angular/forms';
import { CardService } from 'src/app/core/http/card.service';
import { ListService } from 'src/app/core/http/list.service';

@Component({
  selector: 'bbc-card-add',
  templateUrl: './card-add.component.html',
  styleUrls: ['./card-add.component.scss']
})
export class CardAddComponent implements OnInit {
  @Input() listId: string;
  @Output() cardCreated: EventEmitter<void> = new EventEmitter<void>();
  @Output() cancelCreation: EventEmitter<void> = new EventEmitter<void>();

  cardNameForm: FormControl = new FormControl('');

  constructor(private cardService: CardService, private listService: ListService) { }

  ngOnInit(): void {
  }

  createCard() {
    const cardName: string = this.cardNameForm.value;

    if (cardName) {
      this.cardService.create(cardName).subscribe(createdCard => {
        this.listService.addCard(this.listId, createdCard);
        this.cardCreated.next();
      });
    }
  }

  closeForm() {
    this.cancelCreation.next();
  }
}
