import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Card } from 'src/app/shared/models/card.model';
import { CdkDragDrop, moveItemInArray, transferArrayItem } from '@angular/cdk/drag-drop';

@Component({
  selector: 'bbc-card-list',
  templateUrl: './card-list.component.html',
  styleUrls: ['./card-list.component.scss']
})
export class CardListComponent implements OnInit {
  @Output() cardMovedIn: EventEmitter<Card> = new EventEmitter<Card>();
  @Input() cards: Card[] = [];
  @Input() listId: string;
  @Input() connectedListsIds: string[] = [];

  constructor() { }

  ngOnInit(): void {
  }

  onDrag(event: CdkDragDrop<string[]>) {
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else {
      transferArrayItem(event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex);

      this.cardMovedIn.emit(event.item.data);
    }
  }

}
