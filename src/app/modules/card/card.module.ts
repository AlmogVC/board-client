import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CardRoutingModule } from './card-routing.module';
import { CardComponent } from './components/card/card.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { LabelModule } from '../label/label.module';
import { AssigneeModule } from '../assignee/assignee.module';
import { CardListComponent } from './components/card-list/card-list.component';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { CardAddComponent } from './components/card-add/card-add.component';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [CardComponent, CardListComponent, CardAddComponent],
  imports: [
    CommonModule,
    SharedModule,
    CardRoutingModule,
    LabelModule,
    AssigneeModule,
    DragDropModule,
    ReactiveFormsModule,
  ],
  exports: [CardComponent, CardListComponent, CardAddComponent],
})
export class CardModule { }
