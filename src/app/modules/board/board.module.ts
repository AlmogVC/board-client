import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BoardRoutingModule } from './board-routing.module';
import { BoardPage } from './pages/board/board.page';
import { BoardsPage } from './pages/boards/boards.page';
import { ListModule } from '../list/list.module';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { MatSidenavModule } from '@angular/material/sidenav'; 
import { SharedModule } from 'src/app/shared/shared.module';
import { BoardActionsComponent } from './components/board-actions/board-actions.component';
import { BoardToolbarComponent } from './components/board-toolbar/board-toolbar.component';
import { BoardListComponent } from './components/board-list/board-list.component';
import { BoardListItemComponent } from './components/board-list-item/board-list-item.component';


@NgModule({
  declarations: [BoardPage, BoardsPage, BoardActionsComponent, BoardToolbarComponent, BoardListComponent, BoardListItemComponent],
  imports: [
    CommonModule,
    SharedModule,
    BoardRoutingModule,
    ListModule,
    DragDropModule,
    MatSidenavModule,
  ],
  exports: [BoardListComponent]
})
export class BoardModule { }
