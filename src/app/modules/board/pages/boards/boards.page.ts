import { Component, OnInit, OnChanges } from '@angular/core';
import { Board } from 'src/app/shared/models/board.model';
import { BoardService } from 'src/app/core/http/board.service';
import { BackgroundService } from 'src/app/core/services/background.service';

@Component({
  templateUrl: './boards.page.html',
  styleUrls: ['./boards.page.scss']
})
export class BoardsPage implements OnInit {
  boards: Board[] = [];

  constructor(private boardService: BoardService, backgroundService: BackgroundService) {
    backgroundService.setBackgroundColor('#fafbfc');
  }

  ngOnInit(): void {
    this.boardService.getMany().subscribe(boards => {
      this.boards = boards;
    });
  }
}
