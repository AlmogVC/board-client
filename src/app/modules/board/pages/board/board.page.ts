import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Board } from 'src/app/shared/models/board.model';
import { BoardService } from 'src/app/core/http/board.service';
import { BackgroundService } from 'src/app/core/services/background.service';
import { ListService } from 'src/app/core/http/list.service';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { Subscription } from 'rxjs';
import { MatDrawer } from '@angular/material/sidenav';


@Component({
  templateUrl: './board.page.html',
  styleUrls: ['./board.page.scss']
})
export class BoardPage implements OnInit, OnDestroy {
  board: Board;
  routeSubscription: Subscription;

  @ViewChild('menu') public menu: MatDrawer;

  constructor(
    private boardService: BoardService,
    private backgroundService: BackgroundService,
    private listService: ListService,
    private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.routeSubscription = this.route.params.subscribe(params => {
      this.boardService.getById(params['id']).subscribe(board => {
        this.board = board;
      });
   });

    this.backgroundService.setBackgroundImage(this.board.cover);
  }

  addList(listName: string) {
    this.listService.create(listName).subscribe(newList => {
      this.listService.getMany().subscribe(lists => {
        this.board.lists = [...lists];
      });
    });
  }

  ngOnDestroy(): void {
    this.routeSubscription.unsubscribe();
  }

  onMenuToggle() {
    this.menu.toggle();
  }
}
