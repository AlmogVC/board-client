import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BoardPage } from './pages/board/board.page';
import { BoardsPage } from './pages/boards/boards.page';


const routes: Routes = [
  {
    path: 'board/:id',
    component: BoardPage,
    children: [
      {
        path: 'card/:cardId',
        outlet: 'card',
        loadChildren: () => import('../card-dialog/card-dialog.module').then(m => m.CardDialogModule),
      }
    ],
  },
  {
    path: 'boards',
    component: BoardsPage,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BoardRoutingModule { }
