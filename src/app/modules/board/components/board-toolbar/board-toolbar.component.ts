import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Board } from 'src/app/shared/models/board.model';

@Component({
  selector: 'bbc-board-toolbar',
  templateUrl: './board-toolbar.component.html',
  styleUrls: ['./board-toolbar.component.scss']
})
export class BoardToolbarComponent implements OnInit {
  @Output() menuToggle: EventEmitter<void> = new EventEmitter<void>();
  @Input() board: Board;

  constructor() { }

  ngOnInit(): void {
  }

  toggleMenu() {
    this.menuToggle.emit();
  }

}
