import { Component, OnInit, Input } from '@angular/core';
import { Board } from 'src/app/shared/models/board.model';

@Component({
  selector: 'bbc-board-list-item',
  templateUrl: './board-list-item.component.html',
  styleUrls: ['./board-list-item.component.scss']
})
export class BoardListItemComponent implements OnInit {
  @Input() board: Board;

  constructor() { }

  ngOnInit(): void {
  }

}
