import { Component, OnInit, Input } from '@angular/core';
import { Board } from 'src/app/shared/models/board.model';

@Component({
  selector: 'bbc-board-actions',
  templateUrl: './board-actions.component.html',
  styleUrls: ['./board-actions.component.scss']
})
export class BoardActionsComponent implements OnInit {
  @Input() board: Board;

  constructor() { }

  ngOnInit(): void {
  }

}
