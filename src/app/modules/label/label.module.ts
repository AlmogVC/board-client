import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LabelListComponent } from './components/label-list/label-list.component';
import { LabelComponent } from './components/label/label.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { StaticLabelListComponent } from './components/static-label-list/static-label-list.component';
import { StaticLabelComponent } from './components/static-label/static-label.component';
import { MinimalisticStaticLabelComponent } from './components/minimalistic-static-label/minimalistic-static-label.component';
import { MinimalisticStaticLabelListComponent } from './components/minimalistic-static-label-list/minimalistic-static-label-list.component';



@NgModule({
  declarations: [LabelListComponent, LabelComponent, StaticLabelListComponent, StaticLabelComponent, MinimalisticStaticLabelComponent, MinimalisticStaticLabelListComponent],
  imports: [
    CommonModule,
    SharedModule,
  ],
  exports: [LabelListComponent, StaticLabelListComponent, MinimalisticStaticLabelListComponent],
})
export class LabelModule { }
