import { Component, OnInit, Input } from '@angular/core';
import { Label } from 'src/app/shared/models/label.model';

@Component({
  selector: 'bbc-minimalistic-static-label-list',
  templateUrl: './minimalistic-static-label-list.component.html',
  styleUrls: ['./minimalistic-static-label-list.component.scss']
})
export class MinimalisticStaticLabelListComponent implements OnInit {
  @Input() labels: Label[] = [];
  constructor() { }

  ngOnInit(): void {
  }

}
