import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { MinimalisticStaticLabelListComponent } from './minimalistic-static-label-list.component';

describe('MinimalisticStaticLabelListComponent', () => {
  let component: MinimalisticStaticLabelListComponent;
  let fixture: ComponentFixture<MinimalisticStaticLabelListComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ MinimalisticStaticLabelListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MinimalisticStaticLabelListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
