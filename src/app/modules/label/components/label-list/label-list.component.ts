import { Component, OnInit, Input } from '@angular/core';
import { Label } from 'src/app/shared/models/label.model';

@Component({
  selector: 'bbc-label-list',
  templateUrl: './label-list.component.html',
  styleUrls: ['./label-list.component.scss']
})
export class LabelListComponent implements OnInit {
  @Input() labels: Label[] = [];

  constructor() { }

  ngOnInit(): void {
  }

}
