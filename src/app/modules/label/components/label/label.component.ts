import { Component, OnInit, Input } from '@angular/core';
import { Label } from 'src/app/shared/models/label.model';
import { LabelService } from 'src/app/core/http/label.service';

@Component({
  selector: 'bbc-label',
  templateUrl: './label.component.html',
  styleUrls: ['./label.component.scss']
})
export class LabelComponent implements OnInit {
  @Input() label: Label;
  isNameVisible = false;

  constructor(private labelService: LabelService) {
    this.isNameVisible = labelService.isNameVisible;
  }

  ngOnInit(): void {
    this.labelService.nameVisibilitySubject.subscribe(isNameVisible => {
      this.isNameVisible = isNameVisible;
    });
  }

  onClick(event: MouseEvent) {
    event.stopPropagation();

    this.labelService.nameVisibilitySubject.next(!this.isNameVisible)
  }

}
