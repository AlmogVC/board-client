import { Component, OnInit, Input } from '@angular/core';
import { Label } from 'src/app/shared/models/label.model';

@Component({
  selector: 'bbc-minimalistic-static-label',
  templateUrl: './minimalistic-static-label.component.html',
  styleUrls: ['./minimalistic-static-label.component.scss']
})
export class MinimalisticStaticLabelComponent implements OnInit {
  @Input() label: Label;

  constructor() { }

  ngOnInit(): void {
  }

}
