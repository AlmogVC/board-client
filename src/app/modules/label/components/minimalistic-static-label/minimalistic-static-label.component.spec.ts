import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { MinimalisticStaticLabelComponent } from './minimalistic-static-label.component';

describe('MinimalisticStaticLabelComponent', () => {
  let component: MinimalisticStaticLabelComponent;
  let fixture: ComponentFixture<MinimalisticStaticLabelComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ MinimalisticStaticLabelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MinimalisticStaticLabelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
