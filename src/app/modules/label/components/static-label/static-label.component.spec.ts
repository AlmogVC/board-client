import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { StaticLabelComponent } from './static-label.component';

describe('StaticLabelComponent', () => {
  let component: StaticLabelComponent;
  let fixture: ComponentFixture<StaticLabelComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ StaticLabelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StaticLabelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
