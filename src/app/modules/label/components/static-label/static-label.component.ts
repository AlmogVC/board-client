import { Component, OnInit, Input } from '@angular/core';
import { Label } from 'src/app/shared/models/label.model';

@Component({
  selector: 'bbc-static-label',
  templateUrl: './static-label.component.html',
  styleUrls: ['./static-label.component.scss']
})
export class StaticLabelComponent implements OnInit {
  @Input() label: Label;

  constructor() { }

  ngOnInit(): void {
  }

}
