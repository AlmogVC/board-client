import { Component, OnInit, Input } from '@angular/core';
import { Label } from 'src/app/shared/models/label.model';

@Component({
  selector: 'bbc-static-label-list',
  templateUrl: './static-label-list.component.html',
  styleUrls: ['./static-label-list.component.scss']
})
export class StaticLabelListComponent implements OnInit {
  @Input() labels: Label[] = [];

  constructor() { }

  ngOnInit(): void {
  }

}
