import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { StaticLabelListComponent } from './static-label-list.component';

describe('StaticLabelListComponent', () => {
  let component: StaticLabelListComponent;
  let fixture: ComponentFixture<StaticLabelListComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ StaticLabelListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StaticLabelListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
