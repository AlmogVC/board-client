import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TeamBoardsPage } from './pages/team-boards/team-boards.page';


const routes: Routes = [
  {
    path: 'team/boards',
    component: TeamBoardsPage,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TeamRoutingModule { }
