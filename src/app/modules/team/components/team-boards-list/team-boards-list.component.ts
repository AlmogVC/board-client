import { Component, OnInit, Input } from '@angular/core';
import { Team } from 'src/app/shared/models/team.model';

@Component({
  selector: 'bbc-team-boards-list',
  templateUrl: './team-boards-list.component.html',
  styleUrls: ['./team-boards-list.component.scss']
})
export class TeamBoardsListComponent implements OnInit {
  @Input() teams: Team[] = [];
  constructor() { }

  ngOnInit(): void {
  }

}
