import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { TeamBoardsListComponent } from './team-boards-list.component';

describe('TeamBoardsListComponent', () => {
  let component: TeamBoardsListComponent;
  let fixture: ComponentFixture<TeamBoardsListComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ TeamBoardsListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TeamBoardsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
