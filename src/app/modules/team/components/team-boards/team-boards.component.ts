import { Component, OnInit, Input } from '@angular/core';
import { Team } from 'src/app/shared/models/team.model';

@Component({
  selector: 'bbc-team-boards',
  templateUrl: './team-boards.component.html',
  styleUrls: ['./team-boards.component.scss']
})
export class TeamBoardsComponent implements OnInit {
  @Input() team: Team;

  constructor() { }

  ngOnInit(): void {
  }

}
