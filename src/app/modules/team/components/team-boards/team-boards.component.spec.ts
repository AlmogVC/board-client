import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { TeamBoardsComponent } from './team-boards.component';

describe('TeamBoardsComponent', () => {
  let component: TeamBoardsComponent;
  let fixture: ComponentFixture<TeamBoardsComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ TeamBoardsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TeamBoardsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
