import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TeamRoutingModule } from './team-routing.module';
import { TeamBoardsListComponent } from './components/team-boards-list/team-boards-list.component';
import { TeamBoardsComponent } from './components/team-boards/team-boards.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { BoardModule } from '../board/board.module';
import { TeamBoardsPage } from './pages/team-boards/team-boards.page';


@NgModule({
  declarations: [TeamBoardsListComponent, TeamBoardsComponent, TeamBoardsPage],
  imports: [
    CommonModule,
    SharedModule,
    TeamRoutingModule,
    BoardModule,
  ],
  exports: [TeamBoardsListComponent]
})
export class TeamModule { }
