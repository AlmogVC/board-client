import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { TeamBoardsPage } from './team-boards.page';

describe('TeamBoardsPage', () => {
  let component: TeamBoardsPage;
  let fixture: ComponentFixture<TeamBoardsPage>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ TeamBoardsPage ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TeamBoardsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
