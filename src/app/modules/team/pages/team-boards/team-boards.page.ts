import { Component, OnInit } from '@angular/core';
import { TeamService } from 'src/app/core/http/team.service';
import { Team } from 'src/app/shared/models/team.model';
import { BackgroundService } from 'src/app/core/services/background.service';

@Component({
  templateUrl: './team-boards.page.html',
  styleUrls: ['./team-boards.page.scss']
})
export class TeamBoardsPage implements OnInit {
  teams: Team[] = [];

  constructor(private teamService: TeamService, backgroundService: BackgroundService) {
    backgroundService.setBackgroundColor('#fafbfc');
  }

  ngOnInit(): void {
    this.teamService.getMany().subscribe(teams => {
      this.teams = teams;
    })
  }

}
