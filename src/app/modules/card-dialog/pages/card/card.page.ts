import { Component, OnInit } from '@angular/core';
import { CardDialogComponent } from '../../components/card-dialog/card-dialog.component';
import { MatDialog } from '@angular/material/dialog';
import { Card } from 'src/app/shared/models/card.model';
import { Router, ActivatedRoute } from '@angular/router';
import { CardService } from 'src/app/core/http/card.service';

@Component({
  templateUrl: './card.page.html',
  styleUrls: ['./card.page.scss']
})
export class CardPage implements OnInit {
  card: Card;

  constructor(
    public cardDialog: MatDialog,
    private cardService: CardService,
    private router: Router,
    private route: ActivatedRoute) { }

  ngOnInit(): void {
    const cardId: string = this.route.snapshot.paramMap.get('cardId');

    this.cardService.getById(cardId).subscribe(card => {
      this.openCardDialog(card);
    });
  }

  openCardDialog(card: Card) {
    const dialogRef = this.cardDialog.open(CardDialogComponent, {
      panelClass: 'no-padding-dialog',
      width: '1000px',
      data: card,
    });

    dialogRef.afterClosed().subscribe(() => {
      this.router.navigate(['board/1']);
    });
  }
}
