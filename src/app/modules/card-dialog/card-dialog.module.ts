import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CardDialogRoutingModule } from './card-dialog-routing.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { CardPage } from './pages/card/card.page';
import { CardDialogComponent } from './components/card-dialog/card-dialog.component';
import { MarkdownModule } from 'ngx-markdown';
import { LabelModule } from '../label/label.module';


@NgModule({
  declarations: [CardDialogComponent, CardPage],
  imports: [
    CommonModule,
    SharedModule,
    CardDialogRoutingModule,
    MarkdownModule,
    LabelModule,
  ]
})
export class CardDialogModule { }
